package com.sego.sharesheetapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    private AppCompatButton btnStardard;
    private AppCompatButton btnRich;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**Metodo usado atraves de xml*/
    public void launchStanrdard(View view) {
        launchShareSheetStandard(createUriFromFile());
    }

    /**Metodo usado atraves de xml*/
    public void launchRich(View view) {
        launchShareSheetRichContent(createUriFromFile());
    }

    /**Metodo usado atraves de xml*/
    public void lanchReadPdf(View view) {
        launchIntentView(createUriFromFile());
    }

    /**
     * Aqui crearemos una copia de la ubicación de nuestro archivo pdf que se encuentra en assets
     * para que whatsapp y otras apps puedan consumir nuestro pdf
     * en caso contrario marcara un error al querer enviar el archivo
     * debido a que muchas aplicaciones leén los archivis (File) de la memoria externa
     * y assets esta ubicada en la memoria interna
     *
     * NOTA: para el parametro authority FileProvider.getUriForFile checar comentarios en AndroidManifest
     * */
    private Uri createUriFromFile(){
        AssetManager assetManager = getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(getFilesDir(), "abc.pdf");
        try
        {
            in = assetManager.open("abc.pdf");
            out = openFileOutput(file.getName(), Context.MODE_PRIVATE);

            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e)
        {
            Log.e("tag", e.getMessage());
        }
        return FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);

    }

    /**
     * Lanzaremos un Intent ACTION_SEND normal
     * pero agregando EXTRA_TITLE, FLAG_GRANT_READ_URI_PERMISSION y el title null en createChooser
     * nos mostrara un share sheet con el archivo en cuestion a enviar.
     * NOTA: Para poder observar este tipo de share sheet el dispositivo Android
     * debe de contar con Android 10 (API level 29) de lo contrario se mostrara el
     * share sheet standar
     * **/
    private void launchShareSheetRichContent(Uri uri) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("application/pdf");
        shareIntent.putExtra(Intent.EXTRA_TITLE, "Send message");
        shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, null));
    }

    /**
     * Lanzaremos un Intent ACTION_SEND
     * no nos mostrara el archivo a enviar ni su nombre hasta que
     * hayamos elegido la aplicación a traves de la cual enviaremos el pdf
     * */
    private void launchShareSheetStandard(Uri uri) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("application/pdf");
        startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));
    }

    /**
     * Lanzaremos un Intent ACTION_VIEW
     * el cual abrira un share sheet que nos permitira elegir una app
     * para visualizar nuestros pdf
     * */
    private void launchIntentView(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setDataAndType(
                Uri.parse(uri.toString()),
                "application/pdf");
        startActivity(intent);
    }

    /**
     * Este metodo creara la copia de nuestro archivo de nuestra ubicación original
     * de memoria interna en una externa para que las app del share sheet puedan
     * leér el archivo y procesarlo
     * */
    private void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }


}